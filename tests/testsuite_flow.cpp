/**
 * testsuite_flow.cpp
 */
#define BOOST_TEST_MODULE cpp-flow-test test suite
#include <boost/test/unit_test.hpp>

#include <iostream>
#include <stdexcept>
#include <fstream>
#include <vector>
#include <map>
#include "flow/Flow.hpp"
#include "flow/IterableFlowGenerator.hpp"

#define USE_FLOW

const size_t TEST_SIZE = 1000000;

class A
{
	 int attributeA_;
 public:
	 A() : attributeA_(0) {}
	 A(int a) : attributeA_(a) {}
	 
	 std::string getClassName() const { return "A"; }
	 int getAttributeA() const { return attributeA_; } 

};


bool filterOddA(const A& a)
{
    return a.getAttributeA() % 2;
}

bool filterGEA10(const A& a)
{
    return a.getAttributeA() >= 10;
}



struct FixtureTestFlow
{
    std::vector<A> vA;
    
    FixtureTestFlow()
    {
        for(size_t i=0;i<TEST_SIZE;++i)
        {
            vA.push_back( A(i) );
        }
    }

};

BOOST_FIXTURE_TEST_SUITE( cpp_flow, FixtureTestFlow )


BOOST_AUTO_TEST_CASE( flowFilter )
{
    std::vector<A> vCollect;
#ifdef USE_FLOW
    flow::IterableFlowGenerator<A, std::vector<A>::iterator>  generatorA(vA.begin(), vA.end());
    generatorA
        .filter( filterOddA )
        .filter( filterGEA10 )
        .collect( vCollect );
    generatorA.start();
#else
    for( std::vector<A>::iterator it = vA.begin();it!=vA.end();++it)
    {
        if( ((*it).getAttributeA() % 2) && ((*it).getAttributeA() >= 10) )
        {
            vCollect.push_back(*it);
        }
    }
#endif
    BOOST_CHECK_EQUAL(vCollect.empty(),false);

    for(std::vector<A>::const_iterator iter = vCollect.begin() ; iter != vCollect.end() ; ++iter)
    {
        BOOST_CHECK_EQUAL( ((*iter).getAttributeA() % 2) && ((*iter).getAttributeA() >= 10), true );
    }
}

int mapperA(const A& a)
{
    return a.getAttributeA();
}

bool isOdd(const int& in)
{
    return in % 2;
}

bool isGreaterThan10(const int& in)
{
    return in >= 10;
}

BOOST_AUTO_TEST_CASE( flowMap )
{
    std::vector<int> vCollect;
#ifdef USE_FLOW
    flow::IterableFlowGenerator<A, std::vector<A>::iterator>  generatorA(vA.begin(), vA.end());
    generatorA
        .mapper<int>( mapperA )
        .filter( isGreaterThan10 )
        .filter( isOdd )    
        .collect( vCollect );
    generatorA.start();
#else
    for( std::vector<A>::iterator it = vA.begin();it!=vA.end();++it)
    {
        int attA = (*it).getAttributeA();
        if( (attA % 2) && (attA >= 10) )
        {
            vCollect.push_back(attA);
        }
    }
#endif
    BOOST_CHECK_EQUAL(vCollect.empty(),false);

    for(std::vector<int>::const_iterator iter = vCollect.begin() ; iter != vCollect.end() ; ++iter)
    {
        BOOST_CHECK_EQUAL( ((*iter) % 2) && ((*iter) >= 10), true );
    }
}

BOOST_AUTO_TEST_CASE( flowSwitch )
{
    std::vector<int> vCollectOdd;
    std::vector<int> vCollectEven;
#ifdef USE_FLOW
    flow::Flow<int,int,bool>* pSwitchFlow = NULL;
    flow::IterableFlowGenerator<A, std::vector<A>::iterator>  generatorA(vA.begin(), vA.end());
    generatorA.mapper<int>( mapperA ).createSwitch( isOdd, false, pSwitchFlow )
        .caseSwitch( pSwitchFlow, true ).collect( vCollectOdd )
        .caseSwitch( pSwitchFlow, false ).collect( vCollectEven );
    generatorA.start();
#else
    for( std::vector<A>::iterator it = vA.begin();it!=vA.end();++it)
    {
        if( (*it).getAttributeA() % 2 )
        {
            vCollectOdd.push_back((*it).getAttributeA());
        }
        else
        {
            vCollectEven.push_back((*it).getAttributeA());
        }
    }
#endif
    BOOST_CHECK_EQUAL(vCollectOdd.empty(),false);

    for(std::vector<int>::const_iterator iter = vCollectOdd.begin() ; iter != vCollectOdd.end() ; ++iter)
    {
        BOOST_CHECK_EQUAL( (*iter) % 2, 1 );
    }

    BOOST_CHECK_EQUAL(vCollectEven.empty(),false);

    for(std::vector<int>::const_iterator iter = vCollectEven.begin() ; iter != vCollectEven.end() ; ++iter)
    {
        BOOST_CHECK_EQUAL( (*iter) % 2, 0 );
    }
}

BOOST_AUTO_TEST_CASE( flowMisconstructed )
{
    flow::IterableFlowGenerator<A, std::vector<A>::iterator>  generatorA(vA.begin(), vA.end());
    generatorA
        .mapper<int>( mapperA )
        .filter( isGreaterThan10 )
        .filter( isOdd );
    BOOST_CHECK_THROW(generatorA.start(),std::runtime_error);

    std::vector<int> vCollect;
    generatorA
        .mapper<int>( mapperA )
        .filter( isGreaterThan10 )
        .collect( vCollect )
        .filter( isOdd );
    BOOST_CHECK_THROW(generatorA.start(),std::runtime_error);

}

BOOST_AUTO_TEST_CASE( flowBranch )
{
    flow::IterableFlowGenerator<A, std::vector<A>::iterator>  generatorA(vA.begin(), vA.end());
    std::vector<int> vCollect;
    size_t counter = 0;
    flow::Flow<int>* pAnchor = NULL;
    generatorA
        .mapper<int>( mapperA )
        .createBranch(pAnchor).collect( vCollect )
        .branch(pAnchor).count( counter );

    BOOST_CHECK_NO_THROW( generatorA.start() );
    BOOST_CHECK_EQUAL( vCollect.size(), counter );
    BOOST_CHECK_EQUAL( counter, vA.size() );
}



BOOST_AUTO_TEST_SUITE_END();

//EOF
