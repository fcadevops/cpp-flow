/**
 * testsuite_flow.cpp
 */
#include <boost/test/unit_test.hpp>
#include <iostream>
#include <stdexcept>
#include <fstream>
#include <vector>
#include <map>
#include "flow/Flow.hpp"
#include "flow/FlowHelperClasses.hpp"
#include "flow/FileToFlow.hpp"
#include "flow/StreamToFlow.hpp"
#include "flow/FlowToFile.hpp"
#include "flow/IterableFlowGenerator.hpp"


struct TextFileFixture
{
    const char* _filename;
    const size_t _nbLines;

    TextFileFixture(): _filename("test.txt"), _nbLines(10) // header + contents
    {
        std::ofstream ostrm(_filename);
        ostrm << "Col_size_t;Col_txt;Col_double" << std::endl;
        for(size_t i = 0; i < (_nbLines - 1);  ++i) 
        {
            ostrm << i << ";" << "line" << i << ";" << 0.55*i << std::endl;
        }
        ostrm.close();
    }
};

BOOST_FIXTURE_TEST_SUITE( cpp_flow_text_file, TextFileFixture )

BOOST_AUTO_TEST_CASE( streamToFlow )
{
    size_t counter;
    std::ifstream strm(_filename);
    flow::StreamToFlow<std::string> generator(strm);
    generator.count(counter);
    generator.start();
    strm.close();
    BOOST_CHECK_EQUAL(counter, _nbLines);
}

BOOST_AUTO_TEST_CASE( fileToFlow )
{
    flow::FileToFlow generator(_filename);
    size_t counter;
    generator.count(counter);
    generator.start();
    BOOST_CHECK_EQUAL(counter, _nbLines);
}

size_t columnSelector( const flow::SplitLineToElement::TOut& in)
{
    return in.first;
}

std::string pairToString(const flow::SplitLineToElement::TOut& in)
{
    return in.second;
}

BOOST_AUTO_TEST_CASE( splitLineToElementData )
{
    std::vector< std::string > column0;
    std::vector< std::string > column1;
    std::vector< std::string > column2;

    flow::FileToFlow generator(_filename);
    flow::Flow<flow::SplitLineToElement::TOut,flow::SplitLineToElement::TOut,size_t>* pSwitchFlow = NULL;
    generator.splitToElement(';').createSwitch(columnSelector, static_cast<size_t>(0), pSwitchFlow )
        .caseSwitch(pSwitchFlow, 0).mapper<std::string>(pairToString).collect(column0)
        .caseSwitch(pSwitchFlow, 1).mapper<std::string>(pairToString).collect(column1)
        .caseSwitch(pSwitchFlow, 2).mapper<std::string>(pairToString).collect(column2);
    generator.start();
    
    BOOST_CHECK_EQUAL(column0.size(), _nbLines);
    BOOST_CHECK_EQUAL(column1.size(), _nbLines);
    BOOST_CHECK_EQUAL(column2.size(), _nbLines);
}


BOOST_AUTO_TEST_CASE( splitLineToVector )
{
    std::vector< std::vector<std::string> > collection;
    flow::FileToFlow generator(_filename);
    generator.split(';').collect(collection);
    generator.start();
    BOOST_CHECK_EQUAL(collection.size(), _nbLines);
}

BOOST_AUTO_TEST_CASE( dumpFlowIntoFile )
{
    const char* tabToWrite[] = {"line1", "line2", "line3"};
    flow::IterableFlowGenerator<const char*, const char**> writer(tabToWrite, tabToWrite + 3);
    writer.end( flow::FlowToFile<const char*>("test.txt") );
    writer.start();

    std::vector<std::string> collection;
    flow::FileToFlow reader("test.txt");
    reader.collect(collection);
    reader.start();

    BOOST_REQUIRE_EQUAL(collection.size(), 3);
    BOOST_CHECK_EQUAL(collection[0], tabToWrite[0]);
    BOOST_CHECK_EQUAL(collection[1], tabToWrite[1]);
    BOOST_CHECK_EQUAL(collection[2], tabToWrite[2]);

}


BOOST_AUTO_TEST_SUITE_END();

//EOF
