/*
 * FileToFlow.hpp
 * 
 * Copyright 2018 Campos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __FILE_TO_FLOW__HEADER__
#define __FILE_TO_FLOW__HEADER__

#include "flow/Flow.hpp"
#include <fstream>

namespace flow
{

/**
 * @brief This class is the starting point of a flow on a text file.
 * 
 * This generator enables one to use flow process on each line of a text file.
 * A line shall finished by a \n
 */
class FileToFlow : public Flow<std::string>
{
public:
    FileToFlow(const char* filename): _filename(filename)
    {
    }
    virtual ~FileToFlow(){}

    virtual void start()
    { 
        this->checkIntegrity();

        std::ifstream strm(_filename);
        std::string line;
        while( std::getline(strm, line) )
        {
            this->execute(line); 
        }

        strm.close();
    }

    virtual void execute(const std::string& in)
    {
        this->_pNext->execute( in );
    }

private:
    const char* _filename;
};
}

#endif