/*
 * FlowBranch.hpp
 * 
 * Copyright 2018 Campos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __FLOW_BRANCH__HEADER__
#define __FLOW_BRANCH__HEADER__

#include "flow/Flow_private.hpp"
#include <vector>

namespace flow
{

/**
 * @brief Use this class if you want to add branch in your flow.
 * 
 * @tparam T : type of the input and output data
 */
template<typename T>
class FlowBranch : public Flow<T,T>
{
    typedef Flow<T,T> Super;
    typedef std::vector<FlowExecute<T>* > T_ListNextFlowNodes;
    T_ListNextFlowNodes   _nextFlowNodes;
    
public:
    FlowBranch() 
    {
    }

    virtual ~FlowBranch()
    {
        for( typename T_ListNextFlowNodes::iterator it = _nextFlowNodes.begin();
            it != _nextFlowNodes.end(); ++it )
        {
            delete *it;
        }
    }


    virtual void setNext(FlowExecute<T>* pNext)
    {
        _nextFlowNodes.push_back( pNext );
    }

    virtual void checkIntegrity()
    {
        if( _nextFlowNodes.size() )
        {
            for( typename T_ListNextFlowNodes::iterator it = _nextFlowNodes.begin();
                it != _nextFlowNodes.end(); ++it )
            {
                this->checkIntegrity_internal(*it);
            }
        }
        else
        {
            this->checkIntegrity_internal(NULL);
        }
        
    }

    virtual void execute(const T& in)
    {  
        for( typename T_ListNextFlowNodes::iterator it = _nextFlowNodes.begin();
                it != _nextFlowNodes.end(); ++it )
        {
            (*it)->execute( in );
        }      
    }
};

}

#endif