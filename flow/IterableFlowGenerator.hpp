/*
 * IterableFlowGenerator.hpp
 * 
 * Copyright 2018 Campos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __ITERABLE_FLOW_GENERATOR__HEADER__
#define __ITERABLE_FLOW_GENERATOR__HEADER__

#include "flow/Flow.hpp"

namespace flow
{

/**
 * @brief This class is the starting point of a flow on a iterable container.
 * 
 * @tparam T : type of the data pointed by the iterator
 * @tparam Iterator : type of the iterator
 */
template<typename T, typename Iterator>
class IterableFlowGenerator : public Flow<T>
{
public:
    IterableFlowGenerator(Iterator begin, Iterator end): _begin(begin), _end(end)
    {
    }
    virtual ~IterableFlowGenerator(){}

    virtual void start()
    { 
        this->checkIntegrity();
        
        while( _begin != _end )
        {
            this->execute(*_begin); 
            ++_begin;
        }
    }

    virtual void execute(const T& in)
    {
        this->_pNext->execute( in );
    }

protected:
    Iterator _begin;
    Iterator _end;
};
}

#endif