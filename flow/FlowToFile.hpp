/*
 * FlowToFile.hpp
 * 
 * Copyright 2018 Campos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __FLOW_TO_FILE__HEADER__
#define __FLOW_TO_FILE__HEADER__

#include <fstream>

namespace flow
{
    template<typename TIn>
    class FlowToFile
    {
        const char* filename_;
        std::ios_base::openmode mode_;
        std::ofstream* pStream_;
        public:
        FlowToFile(const char* filename, std::ios_base::openmode mode = std::fstream::out | std::fstream::trunc)
        : filename_(filename), mode_(mode), pStream_(NULL)
        {
        }

        ~FlowToFile()
        {
            if( pStream_ && pStream_->is_open() )
            {
                pStream_->close();
            }
            delete pStream_;
        }

        void operator()(const TIn& in)
        {
            if( pStream_ == NULL )
            {
                pStream_ = new std::ofstream(filename_, mode_);
            }
            *pStream_ << in << std::endl;
        }
    };
}
#endif