/*
 * Flow.hpp
 * 
 * Copyright 2018 Campos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __FLOW__HEADER__
#define __FLOW__HEADER__

#define __FROM_FLOW_HEADER__
#include "flow/FlowSwitch.hpp"
#include "flow/FlowBranch.hpp"
#include "flow/FlowFilter.hpp"
#include "flow/FlowMapper.hpp"
#include "flow/FlowIterableMapper.hpp"
#include "flow/FlowEnd.hpp"


#endif
