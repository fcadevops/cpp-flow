/*
 * StreamToFlow.hpp
 * 
 * Copyright 2018 Campos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __STREAM_TO_FLOW__HEADER__
#define __STREAM_TO_FLOW__HEADER__

#include "flow/Flow.hpp"
#include <iostream>

namespace flow
{

/**
 * @brief This class enables one to use an input stream as starting point for a flow.
 * 
 * @tparam T : data type extracted from the strem
 */
template<typename T>
class StreamToFlow : public Flow<T>
{
    std::istream& _strm;
public:
    StreamToFlow(std::istream& strm): _strm(strm)
    {
    }
    virtual ~StreamToFlow(){}

    virtual void start()
    { 
        this->checkIntegrity();

        T data;
        do
        {
            _strm >> data;
            if( _strm.good() )
            {
                this->execute(data); 
            }
        }
        while( _strm.good() );

    }

    virtual void execute(const T& in)
    {
        this->_pNext->execute( in );
    }

};
}

#endif