/*
 * FlowSwitch.hpp
 * 
 * Copyright 2018 Campos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __FLOW_SWITCH__HEADER__
#define __FLOW_SWITCH__HEADER__

#include "flow/Flow_private.hpp"
#include <map>

namespace flow
{

/**
 * @brief Use this class if you want to add alternative path in your flow.
 * 
 * @tparam T : type of the input and output data
 * @tparam SwitchCriteria : type of the switch method (can be a functor or a method)
 * @tparam ResultCriteria : type of the result returned by the switch criteria
 */
template<typename T, typename SwitchCriteria, typename ResultCriteria>
class FlowSwitch : public Flow<T,T,ResultCriteria>
{
    typedef Flow<T,T,ResultCriteria> Super;
    typedef std::map<ResultCriteria, FlowExecute<T>* > T_MapNextFlow;
    SwitchCriteria  _criteria;
    T_MapNextFlow   _mapNextFlow;
    
public:
    FlowSwitch(SwitchCriteria criteria) 
        : _criteria(criteria)
    {
    }

    virtual ~FlowSwitch()
    {
        typename T_MapNextFlow::iterator it = _mapNextFlow.begin();
        while( it != _mapNextFlow.end() )
        {
            delete it->second;
            ++it;
        }
    }


    virtual void setNext(FlowExecute<T>* pNext)
    {
        _mapNextFlow.insert( std::make_pair(this->_param, pNext) );
    }

    virtual void checkIntegrity()
    {
        typename T_MapNextFlow::iterator it = _mapNextFlow.begin();
        while( it != _mapNextFlow.end() )
        {
            this->checkIntegrity_internal(it->second);
            ++it;
        }

        if( _mapNextFlow.size() == 0 )
        {
            this->checkIntegrity_internal(NULL);
        }
        
    }

    virtual void execute(const T& in)
    {
        ResultCriteria result = _criteria(in);
  
        typename T_MapNextFlow::iterator it = _mapNextFlow.find(result);
        if( it != _mapNextFlow.end() )
        {
            it->second->execute( in );
        }
        
        
    }
};

}

#endif