/*
 * FlowHelperClasses.hpp
 * 
 * Copyright 2018 Campos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __FLOW__HELPER_CLASSES__HEADER__
#define __FLOW__HELPER_CLASSES__HEADER__

#include <iostream>
#include <sstream>
#include <map>

namespace flow
{

template<typename TIn, typename Container>
struct CollectorActionInsert
{
    void operator()(Container& container, const TIn& in)
    {
        container.insert(container.end(), in);
    }
};

template<typename TIn, typename Container, typename Insert = CollectorActionInsert<TIn, Container> >
class CollectorAction
{
    Insert _insert;
    Container& _container;
public:
    CollectorAction(Container& container) : _container(container){}
    void operator()(const TIn& in)
    {
        _insert(_container, in);
    }
};

template<typename TIn>
struct PrinterAction
{
    void operator()(const TIn& in)
    {
        std::cout << in << std::endl;
    }
};

template<typename TIn>
struct CountAction
{
    size_t& counter_;
    CountAction(size_t& counter)
        :counter_(counter)
    {
        counter_ = 0;
    }
    void operator()(const TIn& in)
    {
        ++counter_;
    }
};

class SplitLineToElement
{
    char delimiter_;
    size_t column_;
    std::stringstream* strm_;
public:
    typedef std::pair<size_t, std::string> TOut;
    SplitLineToElement()
        : delimiter_(';'), column_(0), strm_(NULL)
    {

    }

    SplitLineToElement(char delimiter)
        : delimiter_(delimiter), column_(0), strm_(NULL)
    {

    }

    ~SplitLineToElement()
    {
        delete strm_;
    }

    bool operator()(const std::string& line, TOut& out)
    {
        if( (strm_ == NULL) || (strm_->str() != line) )
        {
            delete strm_;
            strm_ = new std::stringstream(line);
            column_ = 0;
        }
        std::string token;
        bool res = static_cast<bool>(std::getline(*strm_, token, delimiter_));
        out = std::make_pair(column_,token);
        ++column_;
        return res;
    }

};

class SplitLineToVector
{
    char delimiter_;    
public:
    typedef std::vector<std::string> TOut;

    SplitLineToVector(char delimiter)
        : delimiter_(delimiter)
    {

    }

    TOut operator()(const std::string& line)
    {
        TOut res;
        std::stringstream strm(line);
        std::string token;
        while( std::getline(strm, token, delimiter_) )
        {
            res.push_back(token);
        }
        return res;
    }

};

}

#endif
