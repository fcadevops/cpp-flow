/*
 * FlowIterableMapper.hpp
 * 
 * Copyright 2018 Campos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __FLOW_ITERABLE_MAPPER__HEADER__
#define __FLOW_ITERABLE_MAPPER__HEADER__

#include "flow/Flow_private.hpp"

namespace flow
{

/**
 * @brief Use this class to transform a data type to an other
 * 
 * @tparam TIn : type of the incomming data
 * @tparam TOut : type of the outcomming dta
 * @tparam IterableMapper : mapping function (can be either a function or a functor) 
 * with prototype : bool fn(const TIn&, TOut&). This function is called in a loop on
 * the same input data until it returns false.
 */
template<typename TIn, typename TOut, typename IterableMapper>
class FlowIterableMapper : public Flow<TIn,TOut>
{
    IterableMapper _conv;
public:
    FlowIterableMapper(IterableMapper mapper) : _conv(mapper) 
    {
    }
    virtual ~FlowIterableMapper(){}

    virtual void execute(const TIn& in)
    {
        TOut out;
        while( _conv(in, out) )
        {
            this->_pNext->execute( out );
        }
    }
};

}

#endif