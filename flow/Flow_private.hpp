/*
 * Flow_private.hpp
 * 
 * Copyright 2018 Campos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __FLOW__PRIVATE__HEADER__
#define __FLOW__PRIVATE__HEADER__

#ifndef __FROM_FLOW_HEADER__
#error "Don't include this file directly, include flow.hpp"
#endif

#include <vector>
#include <iostream>
#include <stdexcept>

#include "FlowHelperClasses.hpp"

namespace flow
{
template<typename T, typename Filter> class FlowFilter;
template<typename T, typename Action> class FlowEnd;
template<typename TIn, typename TOut, typename Mapper> class FlowMapper; 
template<typename TIn, typename TOut, typename Mapper> class FlowIterableMapper; 
template<typename T, typename SwitchCriteria, typename ResultCriteria> class FlowSwitch;
template<typename T> class FlowBranch;



template<typename T>
class FlowExecute 
{
public:
    virtual ~FlowExecute(){}
    virtual void execute(const T& in) = 0;

    /**
     * @brief Check that the execution flow has been correctly built.
     * 
     * This method check the integrity of an execution flow. In other words,
     * it check that termination nodes haven't any following nodes, and indeed
     * that the execution flow doesn't finished by an intermediate node.
     * 
     * When the flow doesn't comply with these rules, a std::runtime_error
     * is thrown.
     */
    virtual void checkIntegrity() = 0;
};

template<typename TIn, typename TOut = TIn, typename TParam = int>
class Flow : public FlowExecute<TIn>
{   
protected:
    FlowExecute<TOut>*  _pNext;
    TParam              _param;
public:
    Flow() : _pNext(NULL)
    {
    }

    virtual ~Flow()
    {
        delete _pNext;
    }

    void setParam(const TParam& param)
    {
        _param = param;
    }


    /**
     * @brief Standard behaviour, throw an exception if 
     * no following flow node has been attached.
     */
    virtual void checkIntegrity()
    {
        checkIntegrity_internal(_pNext);
    }

    /**
     * @brief This method adds a node to the flow in order to filter flow data, i.e. incoming data
     *        are transfered to the next flow node depending of the result of a given criteria.
     * 
     * @tparam Criteria : type of the criteria. Should respect following 
     * prototype : bool fn(const TIn&)
     * @param criteria : an instance of the criteria. A copy is done.
     * @return Flow<TOut,TOut>&  : the flow containing the new node.
     */
    template<typename Criteria>
    Flow<TOut,TOut>& filter(Criteria criteria)
    {
        Flow<TOut,TOut>* pNext = new FlowFilter<TOut,Criteria>(criteria);
        setNext(pNext);
        return *pNext;
    }

    /**
     * @brief This method adds a node to the flow in order to map the incoming data type to 
     *        another type.
     * 
     * @tparam MappedT : Output data type
     * @tparam MapperFn : Mapping function from incoming to outcoming data type. Prototype shall
     * be : TOut fn(const TIn&)
     * @param mapper : Instance of the mapping function
     * @return Flow<TOut,MappedT>&  : the flow containing the new node.
     */
    template<typename MappedT, typename MapperFn>
    Flow<TOut,MappedT>& mapper(MapperFn mapper)
    {
        Flow<TOut,MappedT>* pNext = new FlowMapper<TOut,MappedT,MapperFn>(mapper);
        setNext(pNext);
        return *pNext;
    }

    /**
     * @brief This method adds a node to the flow in order to map the incoming data type to 
     *        another type. It behaves like a spliting method which turns a single data to 
     *        multiple data.
     * 
     * @tparam MappedT : Output data type
     * @tparam MapperFn : Mapping function from incoming to outcoming data type. Prototype shall
     * be : bool fn(const TIn&, TOut&). This function is called in a loop on the same input data 
     * until it returns false.
     * @param mapper : Instance of the mapping function
     * @return Flow<TOut,MappedT>&  : the flow containing the new node.
     */
    template<typename MappedT, typename MapperFn>
    Flow<TOut,MappedT>& iterableMapper(MapperFn mapper)
    {
        Flow<TOut,MappedT>* pNext = new FlowIterableMapper<TOut,MappedT,MapperFn>(mapper);
        setNext(pNext);
        return *pNext;
    }

    /**
     * @brief This method adds a node which split incoming data into multiple other data according
     *        to a given delimiter.
     * 
     * @param delimiter : separation character
     * @return Flow<TOut, SplitLineToElement::TOut>& : the flow containing the new node.
     */
    Flow<TOut, SplitLineToElement::TOut>& splitToElement(char delimiter)
    {
        SplitLineToElement spliter(delimiter);
        Flow<TOut, SplitLineToElement::TOut>* pNext = new FlowIterableMapper<TOut,SplitLineToElement::TOut,SplitLineToElement>(spliter);
        setNext(pNext);
        return *pNext;
    }

    /**
     * @brief This method adds a node which split incoming data into a vector of the same type.
     * 
     * @param delimiter : separation character
     * @return Flow<TOut, SplitLineToVector::TOut>& : the flow containing the new node.
     */
    Flow<TOut, SplitLineToVector::TOut>& split(char delimiter)
    {
        SplitLineToVector spliter(delimiter);
        Flow<TOut,SplitLineToVector::TOut >* pNext = new FlowMapper<TOut, SplitLineToVector::TOut,SplitLineToVector>(spliter);
        setNext(pNext);
        return *pNext;
    }

    /**
     * @brief Create a Switch node.
     * 
     * @tparam FlowSwitchType 
     * @tparam SwitchCriteria 
     * @tparam ResultCriteria 
     * @param criteria 
     * @param dummy 
     * @param pAnchor 
     * @return FlowSwitchType& 
     */
    template<typename FlowSwitchType, typename SwitchCriteria, typename ResultCriteria>
    FlowSwitchType& createSwitch(SwitchCriteria criteria, ResultCriteria dummy, FlowSwitchType* & pAnchor )
    {  
        pAnchor = new FlowSwitch<TOut, SwitchCriteria, ResultCriteria>(criteria);
        setNext(pAnchor);
        return *pAnchor;
    }

    /**
     * @brief This method adds a case value node to the flow. This case node has to be attached to
     *        a switch node.
     * 
     * @tparam FlowSwitchType : type of the switch node
     * @tparam CriteriaResult : type of the criteria result
     * @param pFlowSwitchPointer : pointer on the switch node
     * @param dummy : a dmmy value used to infer its type
     * @return FlowSwitchType& 
     */
    template<typename FlowSwitchType, typename CriteriaResult>
    FlowSwitchType& caseSwitch(FlowSwitchType* pAnchor, CriteriaResult dummy)
    {  
        pAnchor->setParam(dummy);
        return *pAnchor;
    }

    /**
     * @brief Create a Branch node
     * 
     * @tparam FlowBranchType  : branch node type
     * @param pAnchor : pointer on the branch node
     * @return Flow<TOut>& 
     */
    template<typename FlowBranchType>
    Flow<TOut>& createBranch(FlowBranchType*& pAnchor)
    {
        pAnchor = new FlowBranch<TOut>();
        setNext(pAnchor);
        return *pAnchor;
    }

    /**
     * @brief This method adds a node from which multiple flow can continue.
     * 
     * @tparam FlowBranchType : branch node type
     * @param pAnchor : pointer on the branch node
     * @return Flow<TOut>& 
     */
    template<typename FlowBranchType>
    Flow<TOut>& branch(FlowBranchType*& pAnchor)
    {
        return *pAnchor;
    }

    /**
     * @brief This method adds a termination flow node. When hits, it executes a given action
     *        which prototype is : void fn(const TIn&)
     * 
     * @tparam Action : action type
     * @param action : action instance
     * @return Flow<TOut>& 
     */
    template<typename Action>
    Flow<TOut>& end(Action action)
    {
        Flow<TOut>* pNext = new FlowEnd<TOut,Action>(action);
        setNext( pNext );
        return *pNext;
    }

    /**
     * @brief This method adds a termintion flow node which aims to store incoming data into a 
     *        given container.
     * 
     * @tparam Container : container type. Shall have following methods :
     *  void insert( Iterator, const TIn&)
     *  Iterator end()
     * @param container : instance of the container
     * @return Flow<TOut>& 
     */
    template<typename Container>
    Flow<TOut>& collect(Container& container)
    {
        CollectorAction<TOut,Container> action(container);
        return end( action );
    }

    /**
     * @brief This method adds a termintion flow node which prints every incoming to cout. Hence
     *        function operator<< shall exist for incoming data
     * 
     * @return Flow<TOut>& 
     */
    Flow<TOut>& print()
    {
        return end( PrinterAction<TOut>() );
    }

    /**
     * @brief This method adds a termintion flow node which increment a counter every times it is
     *        hit.
     * 
     * @param counter : reference on a counter
     * @return Flow<TOut>& 
     */
    Flow<TOut>& count(size_t& counter)
    {
        return end( CountAction<TOut>(counter) );
    }

    protected:
    virtual void setNext(FlowExecute<TOut>* pNext)
    {
        if( _pNext )
        {
            delete _pNext;
        }
        _pNext = pNext;
    }

    void checkIntegrity_internal(FlowExecute<TOut>*  pNext)
    {
        if( pNext )
        {
            pNext->checkIntegrity();
        }
        else
        {
            throw std::runtime_error("Current node is an intermediate node, following node should be present");
        }
    }

};


}

#endif
