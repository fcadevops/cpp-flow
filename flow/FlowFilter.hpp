/*
 * FlowFilter.hpp
 * 
 * Copyright 2018 Campos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */

#ifndef __FLOW_FILTER__HEADER__
#define __FLOW_FILTER__HEADER__

#include "flow/Flow_private.hpp"

namespace flow
{

/**
 * @brief Use this class to add a filtering capabitlity in your flow, i.e. 
 * data not validated by a given criteria are not transfered to the next flow class.
 * 
 * @tparam T : type of the incomming data
 * @tparam Filter : filtering function (a functor or a function returning false if the given 
 * should be discarded)
 */
template<typename T, typename Filter>
class FlowFilter : public Flow<T,T>
{
    Filter _accept;
public:
    FlowFilter(Filter accept) : _accept(accept)
    {
    }
    virtual ~FlowFilter(){}

    virtual void execute(const T& in)
    {
        if( _accept(in) )
        {
            this->_pNext->execute( in );
        }
    }
};

}

#endif