/*
 * FlowEnd.hpp
 * 
 * Copyright 2018 Campos
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */

#ifndef __FLOW_END__HEADER__
#define __FLOW_END__HEADER__

#include "flow/Flow_private.hpp"

namespace flow
{

/**
 * @brief Terminating point of a flow. A given action is executed on the incomming data. 
 * No additional flow class should follow (a std::runtime_error is raised in this case)
 * 
 * @tparam T : type of the incomming data
 * @tparam Action : a function or functor
 */
template<typename T, typename Action>
class FlowEnd : public Flow<T,T>
{
    Action _action;
public:
    FlowEnd(Action action) : _action(action)
    {
    }
    virtual ~FlowEnd(){}

    /**
     * @brief Termination node behaviour
     */
    virtual void checkIntegrity()
    {
        if( this->_pNext )
        {
            throw std::runtime_error("Current node is an termination node, following node shouldn't be present");
        }
    }

    virtual void execute(const T& in)
    {
        _action(in);
    }
};

}

#endif