# GCC support can be specified at major, minor, or micro version
# (e.g. 8, 8.2 or 8.2.0).
# See https://hub.docker.com/r/library/gcc/ for all supported GCC
# tags from Docker Hub.
# See https://docs.docker.com/samples/library/gcc/ for more on how to use this image
FROM gcc:latest

LABEL Name=cpp-flow Version=0.0.1

ARG BOOST_VERSION_MAJOR=1
ARG BOOST_VERSION_MINOR=77
ARG BOOST_VERSION_PATCH=0

RUN apt-get -y update && apt-get install -y
RUN apt-get -y install cmake 
RUN apt-get -y install wget \
    && wget --max-redirect 3 https://boostorg.jfrog.io/artifactory/main/release/${BOOST_VERSION_MAJOR}.${BOOST_VERSION_MINOR}.${BOOST_VERSION_PATCH}/source/boost_${BOOST_VERSION_MAJOR}_${BOOST_VERSION_MINOR}_${BOOST_VERSION_PATCH}.tar.gz \
    && mkdir -p /usr/include/boost && tar zxf boost_${BOOST_VERSION_MAJOR}_${BOOST_VERSION_MINOR}_${BOOST_VERSION_PATCH}.tar.gz -C /usr/include/boost --strip-components=1 \
    && cd /usr/include/boost \
    && ./bootstrap.sh --with-libraries=test \
    && ./b2 install

# These commands copy your files into the specified directory in the image
# and set that as the working location
COPY . /usr/src
WORKDIR /usr/src

RUN cmake -E make_directory build
RUN cmake -E chdir build cmake ../CMake 
RUN cmake --build build

# This command runs your application, comment out this line to compile only
CMD ["cmake", "--build", "build", "--target", "test"]